import { Component } from '@angular/core';

import {AddPostPage} from "../addpost/addpost";
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AddPostPage;

  constructor() {

  }
}
