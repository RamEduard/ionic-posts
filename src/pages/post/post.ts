import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

import { RestProvider } from "../../providers/rest/rest";
import { AddPostPage } from "../addpost/addpost";

/**
 * Post Details Page
 */

@Component({
  selector: 'page-post',
  templateUrl: 'post.html',
})
export class PostPage {

  postData:any = {};
  comments:Array<any> = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public restProv: RestProvider
  ) {
    this.postData = navParams.get('post');

    restProv.getComments(this.postData.id)
      .then(comments => {
        // Assign comments
        this.comments = comments;
      });
  }

  edit(event, postId) {
    this.navCtrl.push(AddPostPage, {
      id: postId
    })
  }

}
