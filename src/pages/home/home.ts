import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";

import {PostPage} from "../post/post";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: Array<any> = [];
  users: Array<any> = [];
  filterUser:string = '';

  constructor(
    protected navCtrl: NavController,
    protected restProv: RestProvider,
    protected loadingCtrl: LoadingController
  ) {
    let loader = loadingCtrl.create({
      content: 'Loading all posts...'
    });

    // Show loading
    loader.present();

    // Get users to associate with every post by userId
    restProv.getUsers().then(users => {
      // Assign users
      this.users = users;

      // Get posts
      restProv.getPosts().then(posts => {
        // Hide loading
        loader.dismiss();

        posts.forEach(post => {
          users.forEach(user => {
            // Add user data related to post
            if (user.id === post.userId) {
              post.user = user;

              return true;
            }
          });

          this.posts.unshift(post);
        });
      });
    });
  }

  detail(event, post) {
    this.navCtrl.push(PostPage, {
      post: post
    });
  }

  filterByUser() {
    let loader = this.loadingCtrl.create({
      content: 'Loading all posts...'
    });

    // Show loading
    loader.present();

    // Reset posts
    this.posts = [];

    this.restProv.getPostsByUser(this.filterUser)
      .then(posts => {
        // Hide loading
        loader.dismiss();

        posts.forEach(post => {
          this.users.forEach(user => {
            // Add user data related to post
            if (user.id === post.userId) {
              post.user = user;

              return true;
            }
          });

          this.posts.unshift(post);
        });
      });
  }

}
