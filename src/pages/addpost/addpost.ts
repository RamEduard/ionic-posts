import { Component, ViewChild, ElementRef } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';

import { RestProvider } from "../../providers/rest/rest";

/**
 * Add Post page form
 */

@Component({
  selector: 'page-addpost',
  templateUrl: 'addpost.html',
})
export class AddPostPage {

  post = {
    id: '',
    body: '',
    title: '',
    userId: ''
  };
  users:Array<any> = [];

  isUpdate:Boolean = false;

  constructor(
    private alertCtrl: AlertController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private restProv: RestProvider
  ) {
    let id = navParams.get('id');

    if (id && id > 0) {
      this.isUpdate = true;

      // Get Post by id to be edited
      this.restProv.getPost(id)
        .then(post => {
          this.post = post;
        });

    } else {
      // Get users for be selected
      restProv.getUsers()
        .then(users => this.users = users);
    }
  }

  @ViewChild('body') body: ElementRef;

  resize() {
    this.body.nativeElement.style.height = this.body.nativeElement.scrollHeight + 'px';
  }

  /**
   * Save new post / Update post actions
   * @returns {boolean}
   */
  savePost() {
    if (this.post.userId == '' || this.post.title == '' || this.post.body == '') {
      let alert = this.alertCtrl.create({
        title: 'Fields empty',
        subTitle: 'All fields are required.',
        buttons: ['Dismiss']
      });
      alert.present();

      return false;
    }

    if (!this.isUpdate) {
      // Save Post and then go to home
      this.restProv.addPost(this.post)
        .then(data => {
          // Log the saved value
          console.log(data);

          // Show home
          this.navCtrl.popToRoot();
        });
    } else {
      // Update Post
      this.restProv.editPost(this.post.id, this.post)
        .then(data => {
          // Log the saved value
          console.log(data);

          // Show home
          this.navCtrl.popToRoot();
        });
    }
  }

  /**
   * Delete Post action
   */
  delete() {
    let alert = this.alertCtrl.create({
      title: 'Confirm action',
      message: 'Do you want to delete this post?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Canceled delete post.');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            // Delete the post and return to home page
            this.restProv.deletePost(this.post.id)
              .then(result => {
                // Log the result
                console.log('Deleted post', result);

                // Return to home
                this.navCtrl.popToRoot();
              });
          }
        }
      ]
    });
    alert.present();
  }

}
