import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  RestProvider for call JSON Placeholder Posts
*/
@Injectable()
export class RestProvider {

  private apiUrl = 'https://jsonplaceholder.typicode.com';

  constructor(public http: HttpClient) {

  }

  /**
   * Get comments from postId or all comments
   * @param {number} postId
   * @returns {Promise<Array>}
   */
  getComments(postId:number = 0):Promise<any> {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/comments' + ((postId > 0) ? '?postId=' + postId : ''))
        .subscribe(comments => {
          // Return comments when is resolved the promise
          resolve(comments);
        }, error => {
          // Log if some error occurred
          console.log(error);
        });
    });
  }

  /**
   * Get Posts list
   * @returns {Promise<Array>}
   */
  getPosts(): Promise<any> {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/posts')
        .subscribe(posts => {
          // Return posts data when is resolved the promise
          resolve(posts);
        }, error => {
          // Log the error in console
          console.log(error);
        });
    });
  }

  /**
   * Get Posts by userId
   * @param userId
   * @returns {Promise<any>}
   */
  getPostsByUser(userId):Promise<any> {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/posts?userId=' + userId)
        .subscribe(posts => {
          // Return posts data when is resolved the promise
          resolve(posts);
        }, error => {
          // Log the error in console
          console.log(error);
        });
    });
  }

  /**
   * Get Post by id
   * @param id
   * @returns {Promise<any>}
   */
  getPost(id):Promise<any> {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/posts/' + id)
        .subscribe(post => {
          // Return post data when is resolved the promise
          resolve(post);
        }, error => {
          // Log the error in console
          console.log(error);
        });
    });
  }

  /**
   * Save a new post
   * @param data
   * @returns {Promise<any>}
   */
  addPost(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.post(this.apiUrl + '/posts', data)
        .subscribe(result => {
          // Return result when is resolved
          resolve(result);
        }, error => {
          // if an error occurred return it
          reject(error);
        })
    });
  }

  /**
   * Update Post
   * @param id
   * @param data
   * @returns {Promise<any>}
   */
  editPost(id, data):Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.put(this.apiUrl + '/posts/' + id, data)
        .subscribe(result => {
          // Return result when is resolved
          resolve(result);
        }, error => {
          // if an error occurred return it
          reject(error);
        })
    });
  }

  /**
   * Delete Post
   * @param id
   * @returns {Promise<any>}
   */
  deletePost(id):Promise<any> {
    return new Promise((resolve, reject) => {
      this.http.delete(this.apiUrl + '/posts/' + id)
        .subscribe(result => {
          // Return result when is resolved
          resolve(result);
        }, error => {
          // if an error occurred return it
          reject(error);
        })
    });
  }

  /**
   * Get Users
   * @returns {Promise<Array>}
   */
  getUsers():Promise<any> {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/users')
        .subscribe(users => resolve(users), error => console.log(error));
    });
  }

}
